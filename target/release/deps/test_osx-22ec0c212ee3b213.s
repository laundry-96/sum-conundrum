	.section	__TEXT,__text,regular,pure_instructions
	.private_extern	__ZN3std2rt10lang_start17hf72c53404a29fb4bE
	.globl	__ZN3std2rt10lang_start17hf72c53404a29fb4bE
	.p2align	4, 0x90
__ZN3std2rt10lang_start17hf72c53404a29fb4bE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, -8(%rbp)
	leaq	l___unnamed_1(%rip), %rsi
	leaq	-8(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	__ZN3std2rt19lang_start_internal17hb574fbed1955c3b8E
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hc8423297cd56b771E:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	*(%rdi)
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ops8function6FnOnce9call_once17h65a8676ab4b1a3bfE:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	*%rdi
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ptr13drop_in_place17h27deea1c82973621E:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	8(%rdi), %rax
	movq	16(%rdi), %rcx
	movq	%rcx, (%rax)
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ptr13drop_in_place17h649a8888d5837d29E:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.p2align	4, 0x90
__ZN4core3ptr13drop_in_place17hcc1ae192ea0fb6b4E:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	je	LBB5_1
	movq	(%rdi), %rdi
	shlq	$3, %rsi
	movl	$8, %edx
	popq	%rbp
	jmp	___rust_dealloc
LBB5_1:
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__literal16,16byte_literals
	.p2align	4
LCPI6_0:
	.quad	2
	.quad	2
LCPI6_1:
	.quad	4
	.quad	4
LCPI6_2:
	.quad	6
	.quad	6
LCPI6_3:
	.quad	8
	.quad	8
LCPI6_4:
	.quad	10
	.quad	10
LCPI6_5:
	.quad	12
	.quad	12
LCPI6_6:
	.quad	14
	.quad	14
LCPI6_7:
	.quad	16
	.quad	16
LCPI6_8:
	.quad	18
	.quad	18
LCPI6_9:
	.quad	20
	.quad	20
LCPI6_10:
	.quad	1000000
	.quad	1000000
	.section	__TEXT,__text,regular,pure_instructions
	.p2align	4, 0x90
__ZN8test_osx4main17h21dcf7be45093b5aE:
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, _rust_eh_personality
	.cfi_lsda 16, Lexception0
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	movq	$0, -48(%rbp)
	leaq	-64(%rbp), %r14
	xorl	%r15d, %r15d
	leaq	-56(%rbp), %r12
	movl	$1, %eax
	movq	%rax, %xmm0
	pslldq	$8, %xmm0
	movdqa	%xmm0, -160(%rbp)
	leaq	-144(%rbp), %r13
	.p2align	4, 0x90
LBB6_1:
	movq	$0, -48(%rbp)
	movl	$8000000, %edi
	movl	$8, %esi
	callq	___rust_alloc
	testq	%rax, %rax
	je	LBB6_22
	movq	%rax, %rbx
	incl	%r15d
	movl	$18, %ecx
	movdqa	-160(%rbp), %xmm0
	movdqa	LCPI6_0(%rip), %xmm3
	movdqa	%xmm3, %xmm8
	movdqa	LCPI6_1(%rip), %xmm4
	movdqa	%xmm4, %xmm9
	movdqa	LCPI6_2(%rip), %xmm5
	movdqa	%xmm5, %xmm10
	movdqa	LCPI6_3(%rip), %xmm6
	movdqa	%xmm6, %xmm11
	movdqa	LCPI6_4(%rip), %xmm7
	movdqa	%xmm7, %xmm12
	movdqa	LCPI6_5(%rip), %xmm3
	movdqa	LCPI6_6(%rip), %xmm4
	movdqa	LCPI6_7(%rip), %xmm5
	movdqa	LCPI6_8(%rip), %xmm6
	movdqa	LCPI6_9(%rip), %xmm7
	.p2align	4, 0x90
LBB6_3:
	movdqa	%xmm0, %xmm1
	paddq	%xmm8, %xmm1
	movdqu	%xmm0, -144(%rbx,%rcx,8)
	movdqu	%xmm1, -128(%rbx,%rcx,8)
	movdqa	%xmm0, %xmm1
	paddq	%xmm9, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm10, %xmm2
	movdqu	%xmm1, -112(%rbx,%rcx,8)
	movdqu	%xmm2, -96(%rbx,%rcx,8)
	movdqa	%xmm0, %xmm1
	paddq	%xmm11, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm12, %xmm2
	movdqu	%xmm1, -80(%rbx,%rcx,8)
	movdqu	%xmm2, -64(%rbx,%rcx,8)
	movdqa	%xmm0, %xmm1
	paddq	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm4, %xmm2
	movdqu	%xmm1, -48(%rbx,%rcx,8)
	movdqu	%xmm2, -32(%rbx,%rcx,8)
	movdqa	%xmm0, %xmm1
	paddq	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm6, %xmm2
	movdqu	%xmm1, -16(%rbx,%rcx,8)
	movdqu	%xmm2, (%rbx,%rcx,8)
	paddq	%xmm7, %xmm0
	addq	$20, %rcx
	cmpq	$1000018, %rcx
	jne	LBB6_3
	movq	%rbx, -144(%rbp)
	movdqa	LCPI6_10(%rip), %xmm0
	movdqu	%xmm0, -136(%rbp)
	movq	$8, -72(%rbp)
	movq	$0, 8(%r14)
	movq	$0, (%r14)
	movl	$7999992, %edi
	movl	$8, %esi
	callq	___rust_alloc
	testq	%rax, %rax
	je	LBB6_24
	movq	%rax, -72(%rbp)
	movq	$999999, -64(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	%rax, %rdi
	addq	$8, %rdi
	movl	$1000000, %edx
	movl	$1, %esi
	.p2align	4, 0x90
LBB6_6:
	cmpq	%rsi, %rdx
	jbe	LBB6_7
	movq	(%rbx,%rsi,8), %rcx
	addq	-8(%rbx,%rsi,8), %rcx
	movq	%rcx, -8(%rdi)
	cmpq	$999998, %rsi
	ja	LBB6_12
	movq	-128(%rbp), %rdx
	leaq	1(%rsi), %rcx
	addq	$8, %rdi
	cmpq	%rsi, %rdx
	movq	%rcx, %rsi
	ja	LBB6_6
	jmp	LBB6_8
	.p2align	4, 0x90
LBB6_12:
	xorl	%esi, %esi
	movq	%rax, %rbx
	xorl	%edx, %edx
	testb	$1, %sil
	jne	LBB6_19
	jmp	LBB6_14
	.p2align	4, 0x90
LBB6_21:
	leaq	(%rbx,%rsi,8), %rbx
	addq	(%rcx), %rdx
	movb	$1, %sil
	testb	$1, %sil
	je	LBB6_14
LBB6_19:
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	cmpq	$800, %rcx
	jb	LBB6_15
	leaq	792(%rbx), %rcx
	movl	$100, %esi
	jmp	LBB6_21
	.p2align	4, 0x90
LBB6_14:
	movl	$1, %esi
	movq	%rbx, %rcx
	cmpq	%rdi, %rbx
	jne	LBB6_21
LBB6_15:
	movq	%rdx, -48(%rbp)
	movl	$7999992, %esi
	movl	$8, %edx
	movq	%rax, %rdi
	callq	___rust_dealloc
	movq	-136(%rbp), %rsi
	testq	%rsi, %rsi
	je	LBB6_17
	movq	-144(%rbp), %rdi
	shlq	$3, %rsi
	movl	$8, %edx
	callq	___rust_dealloc
LBB6_17:
	cmpl	$200, %r15d
	jb	LBB6_1
	leaq	-48(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	__ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Display$u20$for$u20$i64$GT$3fmt17h48b2a72b220472b6E@GOTPCREL(%rip), %rax
	movq	%rax, -64(%rbp)
	leaq	l___unnamed_2(%rip), %rax
	movq	%rax, -120(%rbp)
	movq	$2, -112(%rbp)
	leaq	l___unnamed_3(%rip), %rax
	movq	%rax, -104(%rbp)
	movq	$1, -96(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	$1, -80(%rbp)
	leaq	-120(%rbp), %rdi
	callq	__ZN3std2io5stdio6_print17h7e24a737bb756d1dE
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB6_7:
	addq	$-8, %rdi
	movq	%rdi, -120(%rbp)
	leaq	-1(%rsi), %rax
	movq	%rax, -104(%rbp)
Ltmp0:
	leaq	l___unnamed_4(%rip), %rdi
	callq	__ZN4core9panicking18panic_bounds_check17h24d6731b6290c588E
Ltmp1:
	jmp	LBB6_9
LBB6_8:
	addq	$-8, %rdi
	movq	%rdi, -120(%rbp)
	decq	%rcx
	movq	%rcx, -104(%rbp)
Ltmp2:
	leaq	l___unnamed_4(%rip), %rdi
	movq	%rcx, %rsi
	callq	__ZN4core9panicking18panic_bounds_check17h24d6731b6290c588E
Ltmp3:
LBB6_9:
	ud2
LBB6_22:
	movl	$8000000, %edi
	movl	$8, %esi
	callq	__ZN5alloc5alloc18handle_alloc_error17h3be9b4e08a76f6f5E
LBB6_24:
	movl	$7999992, %edi
	movl	$8, %esi
	callq	__ZN5alloc5alloc18handle_alloc_error17h3be9b4e08a76f6f5E
LBB6_23:
Ltmp4:
	movq	%rax, %rbx
	leaq	-120(%rbp), %rdi
	callq	__ZN4core3ptr13drop_in_place17h27deea1c82973621E
	leaq	-72(%rbp), %rdi
	callq	__ZN4core3ptr13drop_in_place17hcc1ae192ea0fb6b4E
	leaq	-144(%rbp), %rdi
	callq	__ZN4core3ptr13drop_in_place17hcc1ae192ea0fb6b4E
	movq	%rbx, %rdi
	callq	__Unwind_Resume
	ud2
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table6:
Lexception0:
	.byte	255
	.byte	255
	.byte	1
	.uleb128 Lcst_end0-Lcst_begin0
Lcst_begin0:
	.uleb128 Lfunc_begin0-Lfunc_begin0
	.uleb128 Ltmp0-Lfunc_begin0
	.byte	0
	.byte	0
	.uleb128 Ltmp0-Lfunc_begin0
	.uleb128 Ltmp3-Ltmp0
	.uleb128 Ltmp4-Lfunc_begin0
	.byte	0
	.uleb128 Ltmp3-Lfunc_begin0
	.uleb128 Lfunc_end0-Ltmp3
	.byte	0
	.byte	0
Lcst_end0:
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.p2align	4, 0x90
_main:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rsi, %rax
	movslq	%edi, %rdx
	leaq	__ZN8test_osx4main17h21dcf7be45093b5aE(%rip), %rcx
	movq	%rcx, -8(%rbp)
	leaq	l___unnamed_1(%rip), %rsi
	leaq	-8(%rbp), %rdi
	movq	%rax, %rcx
	callq	__ZN3std2rt19lang_start_internal17hb574fbed1955c3b8E
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__DATA,__const
	.p2align	3
l___unnamed_1:
	.quad	__ZN4core3ptr13drop_in_place17h649a8888d5837d29E
	.quad	8
	.quad	8
	.quad	__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hc8423297cd56b771E
	.quad	__ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hc8423297cd56b771E
	.quad	__ZN4core3ops8function6FnOnce9call_once17h65a8676ab4b1a3bfE

	.section	__TEXT,__const
l___unnamed_5:
	.byte	0

	.p2align	4
_str.1:
	.ascii	"libcore/slice/mod.rs"

	.section	__DATA,__const
	.p2align	3
l___unnamed_4:
	.quad	_str.1
	.quad	20
	.long	2448
	.long	10

	.section	__TEXT,__const
l___unnamed_6:
	.byte	10

	.section	__DATA,__const
	.p2align	3
l___unnamed_2:
	.quad	l___unnamed_5
	.space	8
	.quad	l___unnamed_6
	.asciz	"\001\000\000\000\000\000\000"

	.section	__TEXT,__const
	.p2align	3
l___unnamed_3:
	.asciz	"\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000 \000\000\000\000\000\000\000\003\000\000\000\000\000\000"


.subsections_via_symbols
