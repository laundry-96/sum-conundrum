fn main() {
    let mut sum: i64  = 0;

    for _ in 0..200 {
        sum = 0;
        let x: Vec<i64> = (0..1_000_000).collect();
        let y: Vec<i64>  = (0..1_000_000 - 1).map(|i| x[i] + x[i+1]).collect();
        sum += y.iter().step_by(100).sum::<i64>();
    }
    
     println!("{}", sum);
}
